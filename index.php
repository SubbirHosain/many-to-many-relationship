<?php
include("db/database.php");
$dbh = new Database();
if (isset($_POST['save'])) {

    $movie_title = $_POST['movie_title'];
    $movie_runtime = $_POST['movie_runtime'];
    $director_name = $_POST['director_name'];

    if (!empty($movie_title) && !empty($movie_runtime) && !empty($director_name)) {

        //first insert the  movie info
        $movie_sql = "INSERT IGNORE INTO movies (movies_title,movies_runtime) values (?,?)";
        $data = array($movie_title, $movie_runtime);

        //casting into integer as lastInsertId returns a string value
        $movie_id = intval($dbh->insertRow($movie_sql, $data));

        //if no duplicate movie enters proceed further
        if ($movie_id) {

            //insert director table data
            $director_sql = "INSERT IGNORE INTO directors (director_name) values (?)";
            $director_data = array($director_name);
            $director_id = intval($dbh->insertRow($director_sql, $director_data));

            //now insert the relational table data with checking
            //if 0 is returned that means director name already exits
            if ($director_id) {

                //if no duplicate director is found
                //insert simply
                $movies_directors_sql = "INSERT INTO movies_directors (movies_id,directors_id) VALUES(?,?)";
                $movies_directors_data = array($movie_id, $director_id);

                if ($dbh->insertRow($movies_directors_sql, $movies_directors_data)) {
                    echo "data inserted into relational table successfully";
                    die();
                }


            } else {
                
                //search id of that associated name and insert
                $director_id_sql = "SELECT id FROM directors WHERE director_name=?";
                $director_name_data = array($director_name);
                $result = $dbh->getRow($director_id_sql, $director_name_data);
                $director_id = $result['id'];

                $movies_directors_sql = "INSERT INTO movies_directors (movies_id,directors_id) VALUES(?,?)";
                $movies_directors_data = array($movie_id, $director_id);

                if ($dbh->insertRow($movies_directors_sql, $movies_directors_data)) {
                    echo "data inserted into relational table successfully";
                    die();
                }
            }

        } else {
            echo "duplicate movie entered!";
            die();
        }


    } else {
        echo "fill the form";
        sleep(1);
        header("location:index.php");
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <form action="" method="post">
                <div class="form-group">
                    <label for="movie_name">Movie Name</label>
                    <input type="text" class="form-control" name="movie_title" id="movie_title"
                           placeholder="movie name">
                </div>
                <div class="form-group">
                    <label for="movie_runtime">Movie Runtime</label>
                    <input type="text" class="form-control" name="movie_runtime" id="movie_runtime"
                           placeholder="runtime">
                </div>
                <div class="form-group">
                    <label for="director_name">Director name</label>
                    <input type="text" class="form-control" name="director_name" id="director_name"
                           placeholder="director name">
                </div>


                <button type="submit" name="save" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <?php

            $sql = "SELECT * FROM movies";
            $result = $dbh->getRows($sql);
            foreach ($result as $row) {
                ?>
                <hr>
                <h2><?php echo $row['movies_title'] ?></h2>
                <p><?php echo $row['movies_runtime'] ?></p>

                <!-- to get the director name we'll be tricky here -->
                <?php
                    //to get the director name we'll be tricky here
                    // without table join we will fetch the associated director name
                    $sql2 = "SELECT directors_id FROM movies_directors WHERE movies_id={$row['id']}";
                    $did = $dbh->getRows($sql2);

                    $did = array_column($did,'directors_id',0);
                    //print_r($did);
                    //loop through directors id's and get the records from directos table
                    for ($i=0;$i<count($did);$i++){

                        $did_sql = "SELECT director_name FROM directors WHERE id ={$did[$i]}";
                        $result_did = $dbh->getRow($did_sql);

                        echo "<span>{$result_did['director_name']}</span>";

                        //if NOT at the end of directors, print comma
                        echo $did[$i] != end($did) ? ', ': '';
                    }


                ?>

                <hr>
            <?php
            }

            ?>

            <?php
                $sql4 = "SELECT m.movies_title, m.movies_runtime, d.director_name FROM movies m
	                      INNER JOIN movies_directors md ON m.id = md.movies_id
	                      INNER JOIN directors d         ON  md.directors_id = d.id";
                $resut_join = $dbh->getRows($sql4);
                var_dump($resut_join);

            ?>

        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>